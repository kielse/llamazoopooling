﻿using UnityEngine;
using System.Collections;

public class PoolObject : MonoBehaviour {

    /// <summary>
    /// PoolObject
    /// 
    /// Please, read instructions at PoolManager.cs first.
    /// This script can be used in any game object that will be controlled by PoolManager.
    /// 1. Drag and drop this script to the Inspector to add to a GameObject
    /// 2. Create a prefab for the object
    /// 3. Define the time to free the object (it will be available for reuse after that)
    /// 
    /// </summary>

    [Tooltip ("Define the time to free the object (it will be available for reuse after that)")]
    public float timeToFreeMe = 3f;

    void Start()
    {
        gameObject.SetActive(false);
    }

    void OnEnable()
    {
        Invoke("Reset", timeToFreeMe);
    }

    void Reset()
    {
        gameObject.SetActive(false);
    }

    void OnDisable()
    {
        //to prevent a "double disable"
        CancelInvoke();
    }
}
