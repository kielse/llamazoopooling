﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    public Transform player;
    Vector3 offset;
    void Start ()
    {
        offset = transform.position - player.position;
    }

    void Update ()
    {
        Vector3 targetCamPos = player.position + offset;
        transform.position = Vector3.Lerp(transform.position, targetCamPos, Time.deltaTime);
    }

}
