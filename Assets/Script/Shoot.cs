﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {
    public float bulletsRate = 0.5f;
    private PoolManager poolManager;

    void Start()
    {
        poolManager = GetComponent<PoolManager>();
        InvokeRepeating("Shooting", bulletsRate, bulletsRate);
    }
    
    void Shooting()
    {
        GameObject go = poolManager.GetFreeObject();
        if (go)
        {
            go.transform.position = transform.position;
            go.transform.rotation = transform.rotation;
        }
    }
}
