﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {
    public float enemyRate = 3f;
    private PoolManager poolManager;

    void Start()
    {
        poolManager = GetComponent<PoolManager>();
        InvokeRepeating("Spawning", enemyRate, enemyRate);
    }

    void Spawning()
    {
        GameObject go = poolManager.GetFreeObject();
        if (go)
        {
            go.transform.position = new Vector3(Random.Range(-40, 40), transform.position.y, Random.Range(-40,40));
            //go.transform.rotation = transform.rotation;
        }
    }
}
